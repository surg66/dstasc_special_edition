local Layouts = GLOBAL.require("map/layouts").Layouts
local StaticLayout = GLOBAL.require("map/static_layout")
local GENERATE_RICH_CHEST = GetModConfigData("GENERATE_RICH_CHEST")
local GENERATE_BENDER = GetModConfigData("GENERATE_BENDER") or "rich"
local GENERATE_BENDER_ANGRY = GetModConfigData("GENERATE_BENDER_ANGRY")

if GENERATE_RICH_CHEST then
    Layouts["bender_rich"] = StaticLayout.Get("map/static_layouts/bender_"..GENERATE_BENDER)

    AddLevelPreInitAny(function(level)
        if level.location ~= "forest" then
            return
        end

        if not level.set_pieces then
            level.set_pieces = {}
        end

        level.set_pieces["bender_rich"] = {count = 1, tasks = {"Mole Colony Rocks", "Kill the spiders", "Killer bees!", "Squeltch", "Forest hunters", "Dig that rock", "Make a Beehat"}}
    end)
end

if GENERATE_BENDER_ANGRY then
    Layouts["bender_angry"] = StaticLayout.Get("map/static_layouts/bender_angry")
    AddLevelPreInitAny(function(level)
        if level.location ~= "forest" then
            return
        end

        if not level.set_pieces then
            level.set_pieces = {}
        end

        level.set_pieces["bender_angry"] = {count = 1, tasks = {"Mole Colony Rocks", "Kill the spiders", "Killer bees!", "Squeltch", "Forest hunters", "Dig that rock", "Make a Beehat"}}
    end)
end
