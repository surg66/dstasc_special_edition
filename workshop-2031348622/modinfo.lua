name                       = "DSTASC Special Edition"
description                = "Special mod for DSTASC servers."
author                     = "surg"
version                    = "1.0.10"
forumthread                = ""
api_version_dst            = 10
icon_atlas                 = "modicon.xml"
icon                       = "modicon.tex"
priority                   = 0
dont_starve_compatible     = false
reign_of_giants_compatible = false
shipwrecked_compatible     = false
hamlet_compatible          = false
dst_compatible             = true
all_clients_require_mod    = true
client_only_mod            = false
server_filter_tags         = {"dstasc"}

local workshop_mod = folder_name and folder_name:find("workshop-") ~= nil
if not workshop_mod then
	name = "[Git] "..name
end

configuration_options =
{
    {
        name    = "CRAFTING_SCULPTURE_BENDER_RICH",
        label   = "Crafting sculpture bender rich",
        hover   = "Sets crafting sculpture bender rich",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = true
    },
    {
        name    = "CRAFTING_SCULPTURE_BENDER_BUDA",
        label   = "Crafting sculpture bender budda",
        hover   = "Sets crafting sculpture bender budda",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = true
    },
    {
        name    = "CRAFTING_SCULPTURE_BENDER_ANGRY",
        label   = "Crafting sculpture bender angry",
        hover   = "Sets crafting sculpture bender angry",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = false
    },
    {
        name    = "GENERATE_RICH_CHEST",
        label   = "Generate rich chest",
        hover   = "Sets generate rich chest",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable",  data = true }
                    },
        default = true
    },
    {
        name    = "GENERATE_BENDER",
        label   = "Generate type bender",
        hover   = "Sets generate type bender",
        options =   {
                        { description = "Rich", data = "rich" },
                        { description = "Buda",  data = "buda" }
                    },
        default = "rich"
    },
    {
        name    = "GENERATE_BENDER_ANGRY",
        label   = "Generate bender angry",
        hover   = "Sets generate bender angry",
        options =   {
                        { description = "Disable", data = false },
                        { description = "Enable", data = true }
                    },
        default = true
    },
}
