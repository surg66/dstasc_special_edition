require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/ui_chest_3x2.zip"),
    Asset("ANIM", "anim/sacred_chest.zip"),
}

local prefabs = {}

local function onopen(inst, data)
    if data ~= nil then
        inst.components.chestknowsriches.doer = data.doer
    else
        inst.components.chestknowsriches.doer = nil
    end

    inst.AnimState:PlayAnimation("open")
    inst.SoundEmitter:PlaySound("dontstarve/wilson/chest_open")

    if inst:HasTag("chestrich") then
        inst:RemoveTag("chestrich")
        local fx = SpawnPrefab("statue_transition")
        if fx ~= nil then
            fx.entity:SetParent(inst.entity)
        end
    end
end 

local function onclose(inst)
    inst.AnimState:PlayAnimation("close")
    inst.AnimState:PushAnimation("closed", false)
    inst.SoundEmitter:PlaySound("dontstarve/wilson/chest_close")

    inst.components.container.canbeopened = false
    inst.components.timer:StartTimer("think", 0.2)
end

local function getstatus(inst)
    return (inst.components.container.canbeopened == false and "LOCKED") or nil
end

local function OnTimerDone(inst, data)
	if data ~= nil then
        if data.name == "think" then
            if inst.components.chestknowsriches:SetExchange() then
                inst.components.timer:StartTimer("locked_pre", 0.2)
            else
                inst.components.chestknowsriches.doer = nil
                inst.components.container.canbeopened = true                
            end            
		elseif data.name == "locked_pre" then
            inst.AnimState:PlayAnimation("hit")
            inst.AnimState:PushAnimation("closed", false)
            inst.components.timer:StartTimer("locked_pst", 0.8)
            inst.SoundEmitter:PlaySound("dontstarve/common/together/chest_retrap")
            local fx = SpawnPrefab("pandorachest_reset")
            if fx ~= nil then
                fx.entity:SetParent(inst.entity)
            end
        elseif data.name == "locked_pst" then
            inst.components.chestknowsriches:DoExchange()            
            inst.components.chestknowsriches.doer = nil
            inst.components.container.canbeopened = true            
		end
	end
end

local function onsave(inst, data)
    data.isfirstfill = inst._isfirstfill
end

local function onload(inst, data)
    if data ~= nil and data.isfirstfill ~= nil then
		inst._isfirstfill = data.isfirstfill
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    inst.MiniMapEntity:SetIcon("sacred_chest.png")

    inst:AddTag("chest")
    inst.AnimState:SetBank("sacred_chest")
    inst.AnimState:SetBuild("sacred_chest")
    inst.AnimState:PlayAnimation("closed")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst._isfirstfill = true

    inst:AddComponent("inspectable")
	inst.components.inspectable.getstatus = getstatus

    inst:AddComponent("container")
    inst.components.container:WidgetSetup("rich_chest")
    inst.components.container.onopenfn = onopen
    inst.components.container.onclosefn = onclose
		
    inst:AddComponent("hauntable")
    inst.components.hauntable.cooldown = TUNING.HAUNT_COOLDOWN_SMALL

    inst:AddComponent("timer")
    inst:ListenForEvent("timerdone", OnTimerDone)
    
    inst:AddComponent("chestknowsriches")

    inst.OnSave = onsave
    inst.OnLoad = onload

    inst:DoTaskInTime(0, function(inst)
        if inst._isfirstfill then
            inst._isfirstfill = false
            inst.components.chestknowsriches:Fill()
        end
    end)
    
    return inst
end

return Prefab("rich_chest", fn, assets, prefabs)
