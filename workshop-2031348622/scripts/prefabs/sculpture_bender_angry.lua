local function DoDropPhysics(inst, x, y, z)
    if inst.Physics ~= nil then
        inst.Physics:Teleport(x, y + 1, z)

        local speed = math.random() * 4 + 2
        local angle = math.random() * 360

        inst.Physics:SetVel(speed * math.cos(angle), math.random() * 2 + 8, speed * math.sin(angle))
    else
        inst.Transform:SetPosition(x, y, z)
    end
end

local function SetPositionNear(inst, pos, radius)
    local x = pos.x
    local z = pos.z
	local test_angles = {}
	local segments = 64

	for i = 1, segments do
		table.insert(test_angles, (2 * PI * i / segments))
	end

	local numangles = #test_angles
	while numangles > 0 do
		local index = math.random(1, numangles)
		local angle = test_angles[index]
		local px, pz = math.cos(angle) * radius, math.sin(angle) * radius
		if TheWorld.Map:IsAboveGroundAtPoint(pos.x + px, 0, pos.z + pz) then
            x = pos.x + px
            z = pos.z + pz
			break
		end
		table.remove(test_angles, index)
		numangles = #test_angles
	end

    if inst ~= nil then
        inst.Transform:SetPosition(x, 0, z)
    end
end

local function onhammered(inst, worker)
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst.components.lootdropper:DropLoot()
    inst:Remove()
end

local function onwork(inst, worker)
    if inst.AnimState:IsCurrentAnimation("anim") then
        inst.AnimState:PlayAnimation("work")
        inst.AnimState:PushAnimation("anim", false)
    end
end

local function abletoaccepttest(inst, item)   
    if item.prefab == "gears" then
        return true
    end

    return false
end

local function ongivenitem(inst, giver, item)
    if item.prefab == "gears" then
        local value = math.random() * 100
        local newitem = nil
        local is_mandrake_planted = false

        if value >= 0 and value < 85 then        -- 85%
            newitem = SpawnPrefab("humanmeat")
        elseif value >= 85 and value < 87 then   -- 2%
            newitem = SpawnPrefab("shroomcake")
        elseif value >= 87 and value < 89 then   -- 2%
            newitem = SpawnPrefab("moqueca")
        elseif value >= 89 and value < 91 then   -- 2%
            newitem = SpawnPrefab("freshfruitcrepes")
        elseif value >= 91 and value < 93 then   -- 2%
            newitem = SpawnPrefab("lobsterdinner")
        elseif value >= 93 and value < 95 then   -- 2%
            newitem = SpawnPrefab("mandrakesoup")
        elseif value == 96 then                  -- 1%
            newitem = SpawnPrefab("honeycomb")
        elseif value == 97 then                  -- 1%
            newitem = SpawnPrefab("batnosehat")
        elseif value == 98 then                  -- 1%
            newitem = SpawnPrefab("mandrake_planted")
            is_mandrake_planted = true
        elseif value == 99 then                  -- 1%
            newitem = SpawnPrefab("jellybean")
            newitem.components.stackable:SetStackSize(3)
        elseif value == 100 then                 -- 1%
            newitem = SpawnPrefab("greengem")    
        end

        if newitem ~= nil then
            if is_mandrake_planted then
                SetPositionNear(newitem, inst:GetPosition(), 4)

                local fx = SpawnPrefab("spawn_fx_medium")
                if fx ~= nil then
                    fx.entity:SetParent(newitem.entity)
                end
            else
                DoDropPhysics(newitem, inst.Transform:GetWorldPosition())
                inst.SoundEmitter:PlaySound("dontstarve/pig/PigKingThrowGold")
            end
        end
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("anim")    
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
end

local function TrySpawnNear(inst, prefab, radius, maxcount)
    if inst == nil then
        return nil
    end

    local count = 0
    local pos = inst:GetPosition()
    for _, v in ipairs(TheSim:FindEntities(pos.x, pos.y, pos.z, radius)) do
        if v.prefab == prefab then
            count = count + 1
        end
    end

    if count < maxcount then
        return SpawnPrefab(prefab)
    end

    return nil
end

local function onhit(inst, data)
    if inst.AnimState:IsCurrentAnimation("anim") then
        inst.AnimState:PlayAnimation("shock")
        inst.AnimState:PushAnimation("anim", false)
        inst.SoundEmitter:PlaySound("dontstarve/common/lightningrod")
    end

    local value = math.random() * 100
    local item = nil
    local chancetbl = {
        {75, function(inst, attacker) return TrySpawnNear(inst, "poop", 8, 10) end},
        {10, function(inst, attacker) return TrySpawnNear(inst, "guano", 8, 10) end},
        {2,  function(inst, attacker) return TrySpawnNear(inst, "messagebottleempty", 8, 10) end},
        {4,  function(inst, attacker) return TrySpawnNear(inst, "compostwrap", 8, 10) end},
        {4,  function(inst, attacker) return TrySpawnNear(inst, "spoiled_fish", 8, 10) end},
        {5,  function(inst, attacker) 
                if attacker ~= nil and attacker:IsValid() and attacker.components.health ~= nil then
                    attacker.components.health:Kill()
                end
                return nil
             end
        },
    }

    local countitemstbl = 0
    for _, v in pairs(chancetbl) do
        countitemstbl = countitemstbl + 1
    end

    if countitemstbl > 0 then
        local start = 0
        for i = 1, countitemstbl do
            if value >= start and value < start + chancetbl[i][1] then
                item = chancetbl[i][2](inst, data.attacker)
                break
            end
            start = start + chancetbl[i][1]
        end
    end

    if item ~= nil then
        DoDropPhysics(item, inst.Transform:GetWorldPosition())
        inst.SoundEmitter:PlaySound("dontstarve/pig/PigKingThrowGold")
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    local minimap = inst.entity:AddMiniMapEntity()
	minimap:SetIcon("bender_rich.tex")

    MakeObstaclePhysics(inst, .5)

    inst.AnimState:SetBank("sculpture_bender_angry")
    inst.AnimState:SetBuild("sculpture_bender_angry")
    inst.AnimState:PlayAnimation("anim")

    inst.entity:SetPristine()

    inst:AddTag("benderstructure")
    inst:AddTag("structure")
    inst:AddTag("houndfriend")
    inst:AddTag("birdblocker")

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onwork)

    inst:AddComponent("trader")
    inst.components.trader:SetAbleToAcceptTest(abletoaccepttest)
    inst.components.trader.acceptnontradable = true
    inst.components.trader.onaccept = ongivenitem

    inst:AddComponent("health")
    inst.components.health:SetInvincible(true)   

    inst:AddComponent("combat")

    inst:ListenForEvent("onbuilt", onbuilt)
    inst:ListenForEvent("blocked", onhit)

    inst:DoPeriodicTask(15, function (inst)
        local x, y, z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x, y, z, 4, {"playerskeleton"})
        for _, ent in pairs(ents) do
            local px, py, pz = ent.Transform:GetWorldPosition()
            SpawnAt("small_puff", Vector3(px, py, pz))
            ent:Remove()
        end
    end)

    return inst
end

local function statuefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    local minimap = inst.entity:AddMiniMapEntity()
	minimap:SetIcon("bender_rich.tex")

    MakeObstaclePhysics(inst, .5)

    inst.AnimState:SetBank("statue_bender_angry")
    inst.AnimState:SetBuild("statue_bender_angry")
    inst.AnimState:PlayAnimation("anim")

    inst.entity:SetPristine()

    inst:AddTag("benderstructure")
    inst:AddTag("structure")
    inst:AddTag("houndfriend")
    inst:AddTag("birdblocker")
    inst:AddTag("antlion_sinkhole_blocker")

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("trader")
    inst.components.trader:SetAbleToAcceptTest(abletoaccepttest)
    inst.components.trader.acceptnontradable = true
    inst.components.trader.onaccept = ongivenitem

    inst:AddComponent("health")
    inst.components.health:SetInvincible(true)   

    inst:AddComponent("combat")

    inst:ListenForEvent("onbuilt", onbuilt)
    inst:ListenForEvent("blocked", onhit)

    inst:DoPeriodicTask(15, function (inst)
        local x, y, z = inst.Transform:GetWorldPosition()
        local ents = TheSim:FindEntities(x, y, z, 4, {"playerskeleton"})
        for _, ent in pairs(ents) do
            local px, py, pz = ent.Transform:GetWorldPosition()
            SpawnAt("small_puff", Vector3(px, py, pz))
            ent:Remove()
        end
    end)

    return inst
end

return Prefab("sculpture_bender_angry", fn),
    Prefab("statue_bender_angry", statuefn),
    MakePlacer("sculpture_bender_angry_placer", "sculpture_bender_angry", "sculpture_bender_angry", "anim", false)
