local TRASH_PREFABS = {"spoiled_food", "houndstooth", "stinger", "twigs", "guano", "poop"}

local function DoDropPhysics(inst, x, y, z)
    if inst.Physics ~= nil then
        inst.Physics:Teleport(x, y + 1, z)

        local speed = math.random() * 4 + 2
        local angle = math.random() * 360

        inst.Physics:SetVel(speed * math.cos(angle), math.random() * 2 + 8, speed * math.sin(angle))
    else
        inst.Transform:SetPosition(x, y, z)
    end
end

local function onitemtaken(inst, picker, loot)
    if inst.components.pickable.caninteractwith then
        inst.components.pickable.caninteractwith = false
        inst.SoundEmitter:PlaySound("dontstarve/common/together/moonbase/moonstaff_place")
        inst.Light:Enable(false)
        inst.Light:SetColour(255/255, 255/255, 255/255)
    end

    inst.AnimState:ClearOverrideSymbol("gem")
end

local function TrySpawnNear(inst, prefab, radius, maxcount)
    if inst == nil then
        return nil
    end

    local count = 0
    local pos = inst:GetPosition()
    for _, v in ipairs(TheSim:FindEntities(pos.x, pos.y, pos.z, radius)) do
        if v.prefab == prefab then
            count = count + 1
        end
    end

    if count < maxcount then
        return SpawnPrefab(prefab)
    end

    return nil
end

local function givetrash(inst, giver, trashname)
local value = math.random() * 100
    local item = nil
    local chancetbl = {
        {80, function(inst) return nil end},
        {4,  function(inst) return TrySpawnNear(inst, "bluegem", 8, 10) end},
        {4,  function(inst) return TrySpawnNear(inst, "redgem", 8, 10) end},
        {2,  function(inst) return TrySpawnNear(inst, "purplegem", 8, 10) end},
        {1,  function(inst) return TrySpawnNear(inst, "orangegem", 8, 10) end},
        {1,  function(inst) return TrySpawnNear(inst, "yellowgem", 8, 10) end},        
        {1,  function(inst) return TrySpawnNear(inst, "greengem", 8, 10) end},
        {7, function(inst) return nil end},
    }

    local countitemstbl = 0
    for _, v in pairs(chancetbl) do
        countitemstbl = countitemstbl + 1
    end

    if countitemstbl > 0 then
        local start = 0
        for i = 1, countitemstbl do
            if value >= start and value < start + chancetbl[i][1] then
                item = chancetbl[i][2](inst)
                break
            end
            start = start + chancetbl[i][1]
        end
    end

    if item ~= nil then
        DoDropPhysics(item, inst.Transform:GetWorldPosition())
        inst.SoundEmitter:PlaySound("dontstarve/pig/PigKingThrowGold")
    end
end

local function giveitem(inst, itemname)
    inst.components.pickable:SetUp(itemname, 1000000)
    inst.components.pickable:Pause()
    if not inst.components.pickable.caninteractwith then
        inst.components.pickable.caninteractwith = true
        inst.SoundEmitter:PlaySound("dontstarve/common/together/moonbase/moonstaff_place")
    end

    local color = string.gsub(itemname, "gem", "")
    inst.AnimState:ClearOverrideSymbol("gem")
    inst.AnimState:OverrideSymbol("gem", "sculpture_bender_gems", "gem_"..color)

    if color == "red" then
        inst.Light:SetColour(224/255, 111/255, 83/255)
    elseif color == "blue" then
        inst.Light:SetColour(83/255, 152/255, 224/255)
    elseif color == "purple" then
        inst.Light:SetColour(189/255, 83/255, 224/255)
    elseif color == "green" then
        inst.Light:SetColour(83/255, 224/255, 119/255)
    elseif color == "yellow" then
        inst.Light:SetColour(222/255, 224/255, 83/255)
    elseif color == "orange" then
        inst.Light:SetColour(224/255, 139/255, 83/255)
    end

    inst.Light:Enable(true)
end

local function ongivenitem(inst, giver, item)
    local istrashprefab = false
    for _, v in pairs(TRASH_PREFABS) do
        if v == item.prefab then
            istrashprefab = true
            break
        end
    end

    if istrashprefab then
        givetrash(inst, giver, item.prefab)
    else
        giveitem(inst, item.prefab)
    end
end

local function abletoaccepttest(inst, item)   
    for _, v in pairs(TRASH_PREFABS) do
        if v == item.prefab then
            return true
        end
    end

    if inst.components.pickable.caninteractwith then
        return false, "SLOTFULL"
    end

    for _, v in pairs({"redgem", "bluegem", "purplegem", "greengem", "yellowgem", "orangegem"}) do
        if v == item.prefab then
            return true
        end
    end

    return false
end

local function onsave(inst, data)
    data.itemname = inst.components.pickable.caninteractwith and inst.components.pickable.product or nil
end

local function onload(inst, data)
    if data ~= nil then
        if data.itemname then
            giveitem(inst, data.itemname)
        end
    end
end

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()

    if inst.components.pickable.caninteractwith and inst.components.pickable.product ~= nil then
        local lootgem = SpawnPrefab(inst.components.pickable.product)
        DoDropPhysics(lootgem, inst.Transform:GetWorldPosition())
    end

    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("metal")
    inst:Remove()
end

local function onwork(inst, worker)
    inst.AnimState:PlayAnimation("work")
    inst.AnimState:PushAnimation("anim", false)
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("anim")    
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    inst.entity:AddLight()
    
    local minimap = inst.entity:AddMiniMapEntity()
	minimap:SetIcon("bender_rich.tex")

    MakeObstaclePhysics(inst, .5)

    inst.AnimState:SetBank("sculpture_bender_rich")
    inst.AnimState:SetBuild("sculpture_bender_rich")
    inst.AnimState:PlayAnimation("anim")

    inst.entity:SetPristine()

    inst:AddTag("benderstructure")
    inst:AddTag("structure")
    inst:AddTag("birdblocker")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.Light:Enable(false)
    inst.Light:SetRadius(5)
    inst.Light:SetFalloff(.9)
    inst.Light:SetIntensity(.65)
    inst.Light:SetColour(255/255, 255/255, 255/255)

    inst:AddComponent("inspectable")
    
    inst:AddComponent("lootdropper")
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)
    inst.components.workable:SetOnWorkCallback(onwork) 
    
    inst:AddComponent("pickable")
    inst.components.pickable.caninteractwith = false
    inst.components.pickable.onpickedfn = onitemtaken
    inst.components.pickable.paused = true

    inst:AddComponent("trader")
    inst.components.trader:SetAbleToAcceptTest(abletoaccepttest)
    inst.components.trader.acceptnontradable = true
    inst.components.trader.onaccept = ongivenitem

    inst:ListenForEvent("onbuilt", onbuilt)

    inst.OnSave = onsave
    inst.OnLoad = onload

    return inst
end

local function statuefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    inst.entity:AddLight()
    
    local minimap = inst.entity:AddMiniMapEntity()
	minimap:SetIcon("bender_rich.tex")

    MakeObstaclePhysics(inst, .5)

    inst.AnimState:SetBank("statue_bender_rich")
    inst.AnimState:SetBuild("statue_bender_rich")
    inst.AnimState:PlayAnimation("anim")

    inst.entity:SetPristine()

    inst:AddTag("benderstructure")
    inst:AddTag("structure")
    inst:AddTag("birdblocker")
    inst:AddTag("antlion_sinkhole_blocker")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.Light:Enable(false)
    inst.Light:SetRadius(5)
    inst.Light:SetFalloff(.9)
    inst.Light:SetIntensity(.65)
    inst.Light:SetColour(255/255, 255/255, 255/255)

    inst:AddComponent("inspectable")
    
    inst:AddComponent("pickable")
    inst.components.pickable.caninteractwith = false
    inst.components.pickable.onpickedfn = onitemtaken
    inst.components.pickable.paused = true

    inst:AddComponent("trader")
    inst.components.trader:SetAbleToAcceptTest(abletoaccepttest)
    inst.components.trader.acceptnontradable = true
    inst.components.trader.onaccept = ongivenitem

    inst.OnSave = onsave
    inst.OnLoad = onload

    return inst
end

return Prefab("sculpture_bender_rich", fn),
    Prefab("statue_bender_rich", statuefn),
    MakePlacer("sculpture_bender_rich_placer", "sculpture_bender_rich", "sculpture_bender_rich", "anim", false)
