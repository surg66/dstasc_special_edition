local brain = require("brains/benderbudabrain")
local prefabs =
{
    "eye_charge",
}

local GEMSLOTS = 2
local BENDER_SANITYAURA = 0.5

local function retargetfn(inst)
    local playertargets = {}
    for i, v in ipairs(AllPlayers) do
        if v.components.combat.target ~= nil then
            playertargets[v.components.combat.target] = true
        end
    end

    return FindEntity(inst, 20,
        function(guy)
            return inst.components.combat:CanTarget(guy)
                and (playertargets[guy] or
                    (guy.components.combat.target ~= nil and guy.components.combat.target:HasTag("player")))
        end,
        { "_combat" }, --see entityreplica.lua
        { "INLIMBO", "player", "benderstructure" }
    )
end

local function shouldKeepTarget(inst, target)
    return target ~= nil
        and target:IsValid()
        and target.components.health ~= nil
        and not target.components.health:IsDead()
        and inst:IsNear(target, 20)
end

local function SetGem(inst, slot, gemname)
    local symbol = "gem"..tostring(slot)
    if gemname == "opalpreciousgem" then
        inst.AnimState:OverrideSymbol(symbol, "gems", "swap_opalgem")
    else
        inst.AnimState:OverrideSymbol(symbol, "gems", "swap_"..gemname)
    end
end

local function UnsetGem(inst, slot)
    local symbol = "gem"..tostring(slot)
    inst.AnimState:ClearOverrideSymbol(symbol)
end

local function HasOneNameAllGems(inst)
    local gemname = nil

    if #inst._gems == GEMSLOTS then
        for i, v in ipairs(inst._gems) do
            if i == 1 then
                gemname = v
            elseif gemname ~= v then
                gemname = nil
            end
        end
    end

    return gemname
end

local function OmSetUp(inst)
    inst.Light:Enable(false)
    
    local onename = HasOneNameAllGems(inst)
    if onename ~= nil then
        if inst.components.sanityaura == nil and (onename == "yellowgem" or onename == "orangegem" or onename == "greengem" or onename == "opalpreciousgem") then
            inst:AddComponent("sanityaura")
            inst.components.sanityaura.aura = BENDER_SANITYAURA
        end

        inst.Light:Enable(true)

        if onename == "redgem" then
            inst.Light:SetColour(224/255, 111/255, 83/255)
        elseif onename == "bluegem" then
            inst.Light:SetColour(83/255, 152/255, 224/255)
        elseif onename == "purplegem" then
            inst.Light:SetColour(189/255, 83/255, 224/255)
        elseif onename == "greengem" then
            inst.Light:SetColour(83/255, 224/255, 119/255)
        elseif onename == "yellowgem" then
            inst.Light:SetColour(222/255, 224/255, 83/255)
        elseif onename == "orangegem" then
            inst.Light:SetColour(224/255, 139/255, 83/255)
        else
            inst.Light:SetColour(255/255, 255/255, 255/255)
        end
    elseif inst.components.sanityaura ~= nil then
        inst:RemoveComponent("sanityaura")
    end
end

local function ItemTradeTest(inst, item)
    if item == nil then
        return false
    elseif string.sub(item.prefab, -3) ~= "gem" then
        return false, "NOTGEM"
    end
    return true
end

local function FlingGem(inst, gemname, slot)
    local pt = inst:GetPosition()
    pt.y = 2.5
    inst.components.lootdropper:SpawnLootPrefab(gemname, pt)
    inst._canattack = false
end

local function DropGems(inst)
    if #inst._gems > 0 then
        for i, v in ipairs(inst._gems) do
            FlingGem(inst, v, i)
        end
    end
end

local function onhammered(inst)
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    DropGems(inst)
    inst.components.lootdropper:DropLoot()    
    inst:Remove()
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("anim")    
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
end

local function onsave(inst, data)
    data.gems = #inst._gems > 0 and inst._gems or nil
end

local function onload(inst, data, ents)
   if data ~= nil then
        if data.gems ~= nil and #inst._gems < GEMSLOTS then
            for i, v in ipairs(data.gems) do
                table.insert(inst._gems, v)
                SetGem(inst, #inst._gems, v)
            end
            local numgems = #inst._gems
            if numgems > 0 then
                local gemname = inst._gems[numgems]
                if numgems >= GEMSLOTS then
                    inst.components.trader:Disable()
                    local onename = HasOneNameAllGems(inst)
                    if onename == "opalpreciousgem" then
                        inst._canattack = true
                    end
                end

                inst.components.pickable:SetUp(gemname, 1000000)
                inst.components.pickable:Pause()
                inst.components.pickable.caninteractwith = true
                inst.components.pickable.canbepicked = true
            end
            OmSetUp(inst)
        end
    end
end

local function OnGemGiven(inst, giver, item)
    local gemname = item.prefab

    if #inst._gems < GEMSLOTS then
        table.insert(inst._gems, gemname)

        inst.components.pickable:SetUp(gemname, 1000000)
        inst.components.pickable:Pause()
        inst.components.pickable.caninteractwith = true
        inst.components.pickable.canbepicked = true

        SetGem(inst, #inst._gems, gemname)
        OmSetUp(inst)

        if #inst._gems >= GEMSLOTS then
            inst.components.trader:Disable()

            local onename = HasOneNameAllGems(inst)
            if onename == "opalpreciousgem" then
                inst._canattack = true
            end
        end

        inst.SoundEmitter:PlaySound("dontstarve/common/telebase_gemplace")
    end
end

local function OnGemTaken(inst, data)
    local numgems = #inst._gems
    if numgems > 0 then
        table.remove(inst._gems, numgems)
        UnsetGem(inst, numgems)
        inst.SoundEmitter:PlaySound("dontstarve/common/telebase_gemplace")
    end

    inst._canattack = false

    numgems = #inst._gems

    if numgems > 0 then
        inst.components.pickable:ChangeProduct(inst._gems[numgems])
        inst.components.pickable:Pause()
        inst.components.pickable.caninteractwith = true
        inst.components.pickable.canbepicked = true
    else
        inst.components.pickable.caninteractwith = false
        inst.components.pickable.canbepicked = false
    end

    OmSetUp(inst)

    if numgems < GEMSLOTS then
        inst.components.trader:Enable()
    end
end

local function EquipWeapon(inst)
    if inst.components.inventory and not inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS) then
        local weapon = CreateEntity()
        --[[Non-networked entity]]
        weapon.entity:AddTransform()
        weapon:AddComponent("weapon")
        weapon.components.weapon:SetDamage(inst.components.combat.defaultdamage)
        weapon.components.weapon:SetRange(inst.components.combat.attackrange, inst.components.combat.attackrange+4)
        weapon.components.weapon:SetProjectile("eye_charge")
        weapon:AddComponent("inventoryitem")
        weapon.persists = false
        weapon.components.inventoryitem:SetOnDroppedFn(weapon.Remove)
        weapon:AddComponent("equippable")
        
        inst.components.inventory:Equip(weapon)
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()
    inst.entity:AddLight()

    MakeObstaclePhysics(inst, .5)

    inst:AddTag("benderstructure")
    inst:AddTag("structure")
    inst:AddTag("gemsocket")
    inst:AddTag("birdblocker")

    --trader (from trader component) added to pristine state for optimization
    inst:AddTag("trader")

    inst.AnimState:SetBank("sculpture_bender_buda")
    inst.AnimState:SetBuild("sculpture_bender_buda")
    inst.AnimState:PlayAnimation("anim")
    
    inst.MiniMapEntity:SetIcon("bender_buda.tex")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("inventory")
    inst.components.inventory.maxslots = GEMSLOTS

    inst:AddComponent("trader")
    inst.components.trader:SetAbleToAcceptTest(ItemTradeTest)
    inst.components.trader.onaccept = OnGemGiven
    
    inst:AddComponent("pickable")
    inst.components.pickable.caninteractwith = true
    inst.components.pickable.paused = true

    inst:AddComponent("lootdropper")
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammered)

    inst:AddComponent("combat")
    inst.components.combat:SetRange(TUNING.EYETURRET_RANGE)
    inst.components.combat:SetDefaultDamage(TUNING.EYETURRET_DAMAGE)
    inst.components.combat:SetAttackPeriod(31*FRAMES)
    inst.components.combat:SetRetargetFunction(1, retargetfn)
    inst.components.combat:SetKeepTargetFunction(shouldKeepTarget)

    inst.Light:Enable(false)
    inst.Light:SetRadius(5)
    inst.Light:SetFalloff(.9)
    inst.Light:SetIntensity(.65)
    inst.Light:SetColour(255/255, 255/255, 255/255)

    inst:ListenForEvent("onbuilt", onbuilt)
    inst:ListenForEvent("ondeconstructstructure", DropGems)
    inst:ListenForEvent("picked", OnGemTaken)

    inst:SetStateGraph("SGbenderbuda")
    inst:SetBrain(brain)

    inst:DoTaskInTime(0, EquipWeapon)

    inst.OnSave = onsave
    inst.OnLoad = onload

    inst._canattack = false
    inst._gems = {}

    return inst
end

local function statuefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()
    inst.entity:AddLight()

    MakeObstaclePhysics(inst, .5)

    inst:AddTag("benderstructure")
    inst:AddTag("structure")
    inst:AddTag("gemsocket")
    inst:AddTag("birdblocker")
    inst:AddTag("antlion_sinkhole_blocker")

    --trader (from trader component) added to pristine state for optimization
    inst:AddTag("trader")

    inst.AnimState:SetBank("statue_bender_buda")
    inst.AnimState:SetBuild("statue_bender_buda")
    inst.AnimState:PlayAnimation("anim")
    
    inst.MiniMapEntity:SetIcon("bender_buda.tex")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("inventory")
    inst.components.inventory.maxslots = GEMSLOTS

    inst:AddComponent("trader")
    inst.components.trader:SetAbleToAcceptTest(ItemTradeTest)
    inst.components.trader.onaccept = OnGemGiven
    
    inst:AddComponent("pickable")
    inst.components.pickable.caninteractwith = true
    inst.components.pickable.paused = true

    inst:AddComponent("combat")
    inst.components.combat:SetRange(TUNING.EYETURRET_RANGE)
    inst.components.combat:SetDefaultDamage(TUNING.EYETURRET_DAMAGE)
    inst.components.combat:SetAttackPeriod(31*FRAMES)
    inst.components.combat:SetRetargetFunction(1, retargetfn)
    inst.components.combat:SetKeepTargetFunction(shouldKeepTarget)

    inst.Light:Enable(false)
    inst.Light:SetRadius(5)
    inst.Light:SetFalloff(.9)
    inst.Light:SetIntensity(.65)
    inst.Light:SetColour(255/255, 255/255, 255/255)

    inst:ListenForEvent("ondeconstructstructure", DropGems)
    inst:ListenForEvent("picked", OnGemTaken)

    inst:SetStateGraph("SGbenderbuda")
    inst:SetBrain(brain)

    inst:DoTaskInTime(0, EquipWeapon)

    inst.OnSave = onsave
    inst.OnLoad = onload

    inst._canattack = false
    inst._gems = {}

    return inst
end

return Prefab("sculpture_bender_buda", fn, {}, prefabs),
    Prefab("statue_bender_buda", statuefn, {}, prefabs),
    MakePlacer("sculpture_bender_buda_placer", "sculpture_bender_buda", "sculpture_bender_buda", "anim", false)
