require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/cartography_desk.zip"),
}

local prefabs = {}

local function OnUse(inst, doer)
	doer:ShowPopUp(POPUPS.COOKBOOK, true)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, .4)

    inst.MiniMapEntity:SetIcon("cartographydesk.png")

    inst.AnimState:SetBank("cartography_desk")
    inst.AnimState:SetBuild("cartography_desk")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("structure")
    inst:AddTag("simpleusestructure")

    -- MakeSnowCoveredPristine(inst)
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("simpleusestructure")
	inst.components.simpleusestructure.onusefn = OnUse

    return inst
end

return Prefab("richchestdesk", fn, assets, prefabs)
