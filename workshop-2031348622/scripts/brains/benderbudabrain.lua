require "behaviours/standandattack"

local BenderBudaBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

function BenderBudaBrain:OnStart()
    local root = PriorityNode({
        StandAndAttack(self.inst),
    }, .25)

    self.bt = BT(self.inst, root)
end

return BenderBudaBrain
