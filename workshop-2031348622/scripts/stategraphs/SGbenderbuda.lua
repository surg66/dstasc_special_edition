require("stategraphs/commonstates")

local events=
{    
    EventHandler("doattack", function(inst)
        if not inst.sg:HasStateTag("busy") and inst._canattack then 
            inst.sg:GoToState("attack") 
        end
    end),
    EventHandler("worked", function(inst) inst.sg:GoToState("worked") end),
}

local states=
{
    State
    {
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("anim", true)
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State
    {
        name = "worked",
        tags = {"worked", "canrotate"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("work")
        end,

        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },

    State
    {
        name = "attack",
        tags = {"attack", "canrotate"},
        onenter = function(inst)
            inst.AnimState:PlayAnimation("attack")
            inst.SoundEmitter:PlaySound("dontstarve/common/lightningrod")
        end,
        timeline=
        {
            TimeEvent(17 * FRAMES, function(inst) 
                inst.components.combat:StartAttack()
                inst.components.combat:DoAttack()
            end),
        },
        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
}

return StateGraph("benderbuda", states, events, "idle")
