local SimpleUseStructure = Class(function(self, inst)
	self.inst = inst

	self.inst:AddTag("simpleusestructure")
end)

function SimpleUseStructure:OnRemoveFromEntity()
    self.inst:RemoveTag("simpleusestructure")
end

function SimpleUseStructure:Use(doer)
    print("SimpleUseStructure:Use")
	if not CanEntitySeeTarget(doer, self.inst) then
		return false
	end

	if self.onusefn then
		self.onusefn(self.inst, doer)
	end
end

return SimpleUseStructure
