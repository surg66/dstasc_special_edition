local templates = {}

local function GetItem(name, count)
    return {name = name, count = count}
end

local function AddTemplateExchange(items, fn, testfn)
    table.insert(templates, {items = items, fn = fn, testfn = testfn})
end

local function CanSpawnPrefabNear(prefab, pos, radius, maxcount)
    local count = 0
    for _, v in ipairs(TheSim:FindEntities(pos.x, pos.y, pos.z, radius)) do
        if v.prefab == prefab then
            count = count + 1
        end
    end

    if count < maxcount then
        return true
    end

    return false
end

local function SpawnPrefabNear(prefab, pos, radius)
    local x = pos.x
    local z = pos.z
	local test_angles = {}
	local segments = 64

	for i = 1, segments do
		table.insert(test_angles, (2 * PI * i / segments))
	end

	local numangles = #test_angles
	while numangles > 0 do
		local index = math.random(1, numangles)
		local angle = test_angles[index]
		local px, pz = math.cos(angle) * radius, math.sin(angle) * radius
		if TheWorld.Map:IsAboveGroundAtPoint(pos.x + px, 0, pos.z + pz) then
            x = pos.x + px
            z = pos.z + pz
			break
		end
		table.remove(test_angles, index)
		numangles = #test_angles
	end

    local inst = SpawnPrefab(prefab)
    if inst ~= nil then
        inst.Transform:SetPosition(x, 0, z)

        local fx = SpawnPrefab("spawn_fx_medium")
        if fx ~= nil then
            fx.entity:SetParent(inst.entity)
        end
    end
end

AddTemplateExchange({GetItem("guano", 2)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_cave"), 1)
    end)

AddTemplateExchange({GetItem("rocks", 3)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_underrock"), 1)
    end)

AddTemplateExchange({GetItem("pinecone", 1), GetItem("twigs", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_forest"), 1)
    end)

AddTemplateExchange({GetItem("cutgrass", 1), GetItem("petals", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_grass"), 1)
    end)

AddTemplateExchange({GetItem("cutgrass", 1), GetItem("poop", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_savanna"), 1)
    end)

AddTemplateExchange({GetItem("twigs", 1), GetItem("acorn", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_deciduous"), 1)
    end)

AddTemplateExchange({GetItem("rocks", 1), GetItem("boneshard", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_desertdirt"), 1)
    end)

AddTemplateExchange({GetItem("rocks", 1), GetItem("flint", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_rocky"), 1)
    end)

AddTemplateExchange({GetItem("rocks", 1), GetItem("driftwood_log", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_pebblebeach"), 1)
    end)

AddTemplateExchange({GetItem("cutgrass", 1), GetItem("foliage", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_sinkhole"), 1)
    end)

AddTemplateExchange({GetItem("turf_desertdirt", 1), GetItem("ice", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_mud"), 1)
    end)

AddTemplateExchange({GetItem("cutlichen", 1), GetItem("spore_tall", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_fungus"), 1)
    end)

AddTemplateExchange({GetItem("cutlichen", 1), GetItem("spore_medium", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_fungus_red"), 1)
    end)

AddTemplateExchange({GetItem("cutlichen", 1), GetItem("spore_small", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_fungus_green"), 1)
    end)

AddTemplateExchange({GetItem("rocks", 1), GetItem("goldnugget", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("turf_archive"), 1)
    end)

AddTemplateExchange({GetItem("papyrus", 1), GetItem("featherpencil", 1), GetItem("houndstooth", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("chesspiece_claywarg_sketch"), 1)
    end)

AddTemplateExchange({GetItem("papyrus", 1), GetItem("featherpencil", 1), GetItem("redgem", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("chesspiece_clayhound_sketch"), 1)
    end)

AddTemplateExchange({GetItem("cutstone", 2), GetItem("transistor", 2)},
    function(self)
        local recipe = GetValidRecipe("madscience_lab")
        if recipe ~= nil then
            if not self.doer.components.builder:KnowsRecipe(recipe.name) then
                self.doer.components.builder:UnlockRecipe(recipe.name)
            end

            if not self.doer.components.builder:IsBuildBuffered(recipe.name) then
                self.doer.components.builder:BufferBuildForce(recipe.name)
                self.doer.player_classified.learnrecipeevent:push() -- for play sound
            end

            self.doer:PushEvent("refreshcrafting")
        end
    end,
    function(self) -- testfn: can build?
        local recipe = GetValidRecipe("madscience_lab")
        if recipe ~= nil and self.doer.components.builder and not self.doer.components.builder:IsBuildBuffered(recipe.name) then
            return true
        end
        return false
    end)

AddTemplateExchange({GetItem("twigs", 1), GetItem("kelp", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("bullkelp_root"), 1)
    end)

AddTemplateExchange({GetItem("slurtle_shellpieces", 1), GetItem("houndstooth", 10), GetItem("fishmeat", 1)},
    function(self)
        print("fn GiveItem dug_trap_starfish")
        self.container:GiveItem(SpawnPrefab("dug_trap_starfish"), 1)
    end)

AddTemplateExchange({GetItem("twigs", 1), GetItem("spoiled_food", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("dug_berrybush"), 1)
    end)

AddTemplateExchange({GetItem("twigs", 1), GetItem("poop", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("dug_berrybush2"), 1)
    end)

AddTemplateExchange({GetItem("twigs", 1), GetItem("guano", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("dug_berrybush_juicy"), 1)
    end)

AddTemplateExchange({GetItem("carrot", 1)},
    function(self)
        SpawnPrefabNear("bunnyman", self.inst:GetPosition(), 4)
    end,
    function(self) -- testfn: can spawn?
        return CanSpawnPrefabNear("bunnyman", self.inst:GetPosition(), 20, 5)
    end)

AddTemplateExchange({GetItem("meat", 1)},
    function(self)
        SpawnPrefabNear("pigman", self.inst:GetPosition(), 4)
    end,
    function(self) -- testfn: can spawn?
        return CanSpawnPrefabNear("pigman", self.inst:GetPosition(), 20, 5)
    end)

AddTemplateExchange({GetItem("monstermeat", 1)},
    function(self)
        SpawnPrefabNear("worm", self.inst:GetPosition(), 4)
    end,
    function(self) -- testfn: can spawn?
        return CanSpawnPrefabNear("worm", self.inst:GetPosition(), 20, 5)
    end)

AddTemplateExchange({GetItem("stinger", 1)},
    function(self)
        SpawnPrefabNear("bee", self.inst:GetPosition(), 4)
    end,
    function(self) -- testfn: can spawn?
        return CanSpawnPrefabNear("bee", self.inst:GetPosition(), 20, 5)
    end)

AddTemplateExchange({GetItem("petals", 1)},
    function(self)
        SpawnPrefabNear("butterfly", self.inst:GetPosition(), 4)
    end,
    function(self) -- testfn: can spawn?
        return CanSpawnPrefabNear("butterfly", self.inst:GetPosition(), 20, 5)
    end)

AddTemplateExchange({GetItem("goldnugget", 2), GetItem("redgem", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("thulecite_pieces"), 1)
    end)

AddTemplateExchange({GetItem("spoiled_food", 1), GetItem("cutgrass", 1)},
    function(self)
        self.container:GiveItem(SpawnPrefab("dug_grass"), 1)
    end)
--[[
-- ToDo
AddTemplateExchange({GetItem("petals", 1)},
    function(self)
        local inst = SpawnPrefab("butterfly")
        if inst and inst.butterflyspawner ~= nil then
            inst.butterflyspawner:StopTracking(inst)
        end
        self.container:GiveItem(inst, 1)
    end)
]]

local ChestKnowsRiches = Class(function(self, inst)
    self.inst = inst
    self.container = inst.components.container
    self.template = nil
    self.doer = nil
end)

function ChestKnowsRiches:SetExchange()
    self.template = nil

    for _, template in pairs(templates) do
        local found = true

        for _, item in pairs(template.items) do
            if not self.container:Has(item.name, item.count) then
                found = false
                break
            end
        end

        if found then
            if template.testfn ~= nil then
                if template.testfn(self) == false then -- result nil and true -> continue
                    return false
                end
            end

            self.template = template

            return true
        end
    end

    return false
end

function ChestKnowsRiches:DoExchange()
    if self.template ~= nil then
        for _, item in pairs(self.template.items) do
            self.container:ConsumeByName(item.name, item.count)
        end

        self.container:DropEverything()

        if self.template.fn ~= nil then
            self.template.fn(self)
        end    
    end   
end

function ChestKnowsRiches:Fill()
    self.container:DropEverything()

    -- Slot 1
    local slot = 1    
    local value = math.random() * 100
    local item = nil

    if value >= 0 and value < 30 then        -- 30%
        item = SpawnPrefab("ruinshat")
    elseif value >= 30 and value < 55 then   -- 25%
        item = SpawnPrefab("armorruins")
    elseif value >= 55 and value < 80 then   -- 25%
        item = SpawnPrefab("ruins_bat")
    elseif value >= 80 and value < 93 then   -- 13%
        item = SpawnPrefab("thulecite")
        item.components.stackable:SetStackSize(8)
    elseif value >= 93 and value < 98 then   -- 5%
        item = SpawnPrefab("minotaurhorn")
    elseif value >= 98 and value <= 100 then -- 2%
        item = SpawnPrefab("eyeturret_item") 
    end

    if item ~= nil then
        self.container:GiveItem(item, slot)
        slot = slot + 1
    end

    -- Slot 2
    value = math.random() * 100
    item = nil

    if value >= 0 and value < 35 then        -- 35%
        item = SpawnPrefab("yellowstaff")
    elseif value >= 35 and value < 65 then   -- 30%
        item = SpawnPrefab("yellowamulet")
    elseif value >= 65 and value < 90 then   -- 25%
        item = SpawnPrefab("orangestaff")
    elseif value >= 90 and value <= 100 then -- 10%
        item = SpawnPrefab("opalstaff")
    end

    if item ~= nil then
        self.container:GiveItem(item, slot)
        slot = slot + 1
    end

    -- Slot 3    
    local items = {}    

    if math.random() < 0.3 and #items <= 4 then
        item = SpawnPrefab("meatballs")
        item.components.stackable:SetStackSize(5)
        table.insert(items, item)
    end

    if math.random() < 0.3 and #items <= 4 then
        item = SpawnPrefab("honeyham")
        item.components.stackable:SetStackSize(5)
        table.insert(items, item)
    end

    if math.random() < 0.2 and #items <= 4 then
        item = SpawnPrefab("dragonpie")
        item.components.stackable:SetStackSize(5)
        table.insert(items, item)
    end
    
    if math.random() < 0.15 and #items <= 4 then
        item = SpawnPrefab("freshfruitcrepes")
        item.components.stackable:SetStackSize(2)
        table.insert(items, item)
    end

    if math.random() < 0.15 and #items <= 4 then
        item = SpawnPrefab("voltgoatjelly")
        item.components.stackable:SetStackSize(2)
        table.insert(items, item)
    end

    local numitems = #items
    if numitems == 0 then
        item = SpawnPrefab("meatballs")
        item.components.stackable:SetStackSize(5)
        table.insert(items, item)
        table.insert(items, SpawnPrefab("taffy"))
        table.insert(items, SpawnPrefab("taffy"))
        table.insert(items, SpawnPrefab("taffy"))
    else    
        if numitems < 4 then
            for num = 1, (4 - numitems) do
                table.insert(items, SpawnPrefab("taffy"))
            end
        end
    end

    local bundle = SpawnPrefab("gift")
    bundle.components.unwrappable:WrapItems(items)
    for i, v in ipairs(items) do
        v:Remove()
    end

    if item ~= nil then
        self.container:GiveItem(bundle, slot)
    end

    self.inst:AddTag("chestrich")
end

return ChestKnowsRiches
