PrefabFiles = 
{
	"sculpture_bender_angry",
    "sculpture_bender_rich",
    "sculpture_bender_buda",    
    "rich_chest",
    --"richchestdesk",
}

Assets = 
{
	Asset("ANIM", "anim/sculpture_bender_gems.zip"),
    Asset("ANIM", "anim/sculpture_bender_angry.zip"),    
    Asset("ANIM", "anim/sculpture_bender_rich.zip"),
    Asset("ANIM", "anim/sculpture_bender_buda.zip"),
    Asset("ANIM", "anim/statue_bender_angry.zip"),
    Asset("ANIM", "anim/statue_bender_rich.zip"),
    Asset("ANIM", "anim/statue_bender_buda.zip"),
    Asset("IMAGE", "images/inventoryimages/bender_icons.tex"),
	Asset("ATLAS", "images/inventoryimages/bender_icons.xml"),
    Asset("IMAGE", "images/mapicons/bender_icons.tex"),
	Asset("ATLAS", "images/mapicons/bender_icons.xml"),
}

local Ingredient = GLOBAL.Ingredient
local RECIPETABS = GLOBAL.RECIPETABS
local TECH = GLOBAL.TECH
local STRINGS = GLOBAL.STRINGS
local Vector3 = GLOBAL.Vector3
local require = GLOBAL.require

STRINGS.NAMES.SCULPTURE_BENDER_RICH = "Sculpture rich Bender"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SCULPTURE_BENDER_RICH = "I'm going to build my own theme park with blackjack and hookers."
STRINGS.RECIPE_DESC.SCULPTURE_BENDER_RICH = "Sculpture of a rich Bender."
STRINGS.NAMES.STATUE_BENDER_RICH = "Statue rich Bender"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.STATUE_BENDER_RICH = "I'm going to build my own theme park with blackjack and hookers."
STRINGS.RECIPE_DESC.STATUE_BENDER_RICH = "Statue of a rich Bender."

STRINGS.NAMES.SCULPTURE_BENDER_BUDA = "Sculpture Budda Bender"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SCULPTURE_BENDER_BUDA = "Calm only calm."
STRINGS.RECIPE_DESC.SCULPTURE_BENDER_BUDA = "Sculpture of a Budda Bender."
STRINGS.NAMES.STATUE_BENDER_BUDA = "Statue Budda Bender"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.STATUE_BENDER_BUDA = "Calm only calm."
STRINGS.RECIPE_DESC.STATUE_BENDER_BUDA = "Statue of a Budda Bender."

STRINGS.NAMES.SCULPTURE_BENDER_ANGRY = "Sculpture Admin"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SCULPTURE_BENDER_ANGRY = "Bite my shiny metal ass!"
STRINGS.RECIPE_DESC.SCULPTURE_BENDER_ANGRY = "Sculpture Admin"
STRINGS.NAMES.STATUE_BENDER_ANGRY = "Statue Admin"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.STATUE_BENDER_ANGRY = "Bite my shiny metal ass!"
STRINGS.RECIPE_DESC.STATUE_BENDER_ANGRY = "Statue Admin"

STRINGS.NAMES.RICH_CHEST = "Rich Chest"

AddMinimapAtlas("images/mapicons/bender_icons.xml")

local CRAFTING_SCULPTURE_BENDER_ANGRY = GetModConfigData("CRAFTING_SCULPTURE_BENDER_ANGRY")
local CRAFTING_SCULPTURE_BENDER_RICH  = GetModConfigData("CRAFTING_SCULPTURE_BENDER_RICH")
local CRAFTING_SCULPTURE_BENDER_BUDA  = GetModConfigData("CRAFTING_SCULPTURE_BENDER_BUDA")

if CRAFTING_SCULPTURE_BENDER_ANGRY then
    AddRecipe("sculpture_bender_angry", {Ingredient("marble", 20), Ingredient("goldnugget", 20), Ingredient("trinket_6", 10)}, RECIPETABS.TOWN, TECH.SCIENCE_TWO, "sculpture_bender_angry_placer", 2, nil, nil, nil, "images/inventoryimages/bender_icons.xml", "bender_angry.tex")
end

if CRAFTING_SCULPTURE_BENDER_RICH then
    AddRecipe("sculpture_bender_rich", {Ingredient("marble", 20), Ingredient("goldnugget", 20), Ingredient("trinket_6", 10)}, RECIPETABS.TOWN, TECH.SCIENCE_TWO, "sculpture_bender_rich_placer", 2, nil, nil, nil, "images/inventoryimages/bender_icons.xml", "bender_rich.tex")
end

if CRAFTING_SCULPTURE_BENDER_BUDA then
    AddRecipe("sculpture_bender_buda", {Ingredient("marble", 20), Ingredient("goldnugget", 20), Ingredient("trinket_6", 10)}, RECIPETABS.TOWN, TECH.SCIENCE_TWO, "sculpture_bender_buda_placer", 2, nil, nil, nil, "images/inventoryimages/bender_icons.xml", "bender_buda.tex")
end

--[[
AddAction("SIMPLEUSESTRUCTURE", "Use", function(act)
    print("call SIMPLEUSESTRUCTURE 1")
    if act.target ~= nil and act.doer ~= nil then
        print("call SIMPLEUSESTRUCTURE 2")
        if act.target.components.simpleusestructure ~= nil then
            print("call SIMPLEUSESTRUCTURE 3")
			act.target.components.simpleusestructure:Use(act.doer)
			return true
		end
    end
end)

AddComponentAction("SCENE", "simpleusestructure", function(inst, doer, actions, right)
    --print("call SCENE simpleusestructure")
    table.insert(actions, GLOBAL.ACTIONS.SIMPLEUSESTRUCTURE)
end)
]]

--------------------------------------------------------------------------
-- Source modified from containers.lua

local containers = require "containers"
local params = {}
local containers_widgetsetup_base = containers.widgetsetup

function containers.widgetsetup(container, prefab, data, ...)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
    else
        containers_widgetsetup_base(container, prefab, data, ...)
    end
end

local function MakeRichChest()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_chest_3x2",
            animbuild = "ui_chest_3x2",
            pos = Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }

    for y = 1, 0, -1 do
        for x = 0, 2 do
            table.insert(container.widget.slotpos, Vector3(80 * x - 80 * 2 + 80, 80 * y - 80 * 2 + 120, 0))
        end
    end

    return container
end

params.rich_chest = MakeRichChest()

for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
--------------------------------------------------------------------------

AddComponentPostInit("builder", function(self)
    self.BufferBuildForce = function(self, recname)
        local recipe = GLOBAL.GetValidRecipe(recname)
        if recipe ~= nil and recipe.placer ~= nil and not self:IsBuildBuffered(recname) then
            if self:KnowsRecipe(recname) then
                if self.freebuildmode then
                    if not table.contains(self.recipes, recname) and GLOBAL.CanPrototypeRecipe(recipe.level, self.accessible_tech_trees) then
                        self:ActivateCurrentResearchMachine(recipe)
                    end
                elseif not recipe.nounlock then
                    self:AddRecipe(recname)
                end
            elseif GLOBAL.CanPrototypeRecipe(recipe.level, self.accessible_tech_trees) and self:CanLearn(recname) then
                    self:ActivateCurrentResearchMachine(recipe)
                    self:UnlockRecipe(recname)
                else
                    return
                end
            local materials = self:GetIngredients(recname)
            local wetlevel = self:GetIngredientWetness(materials)
            self:RemoveIngredients(materials, recname)
            self.buffered_builds[recname] = wetlevel
            self.inst.replica.builder:SetIsBuildBuffered(recname, true)
        end
    end
end)

for _, v in ipairs({"statue_bender_angry", "statue_bender_buda", "statue_bender_rich"}) do
    AddPrefabPostInit(v, function(inst)
        inst:AddTag("mh_r1")
    end)
end

